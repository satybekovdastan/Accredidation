package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by User on 25.04.2018.
 */

public class Hotel extends RealmObject{

    @SerializedName("object_id")
    @Expose
    private int object_id;

    @SerializedName("object_fio")
    @Expose
    String object_fio;

    @SerializedName("object_name")
    @Expose
    String object_name;

    @SerializedName("comment")
    @Expose
    String comment;

    @SerializedName("object_info")
    @Expose
    private String object_info;

    @SerializedName("is_changed")
    @Expose
    private boolean is_changed;

    private boolean is_upload = false;

    RealmList<Type> typeArrayList = null;

    public void setName(String name) {
        this.object_name = name;
    }

    public String getName() {
        return object_name;
    }

    public void setTypeArrayList(RealmList<Type> typeArrayList) {
        this.typeArrayList = typeArrayList;
    }

    public RealmList<Type> getTypeArrayList() {
        return typeArrayList;
    }

    public void addType(Type type){
        typeArrayList.add(type);
    }

    public Type getType(){
        return typeArrayList.get(0);
    }

    public void setObject_fio(String object_fio) {
        this.object_fio = object_fio;
    }

    public String getObject_fio() {
        return object_fio;
    }

    public String getObject_name() {
        return object_name;
    }


    public String getObject_info() {
        return object_info;
    }

    public void setObject_info(String object_info) {
        this.object_info = object_info;
    }

    public void setObject_name(String object_name) {
        this.object_name = object_name;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public boolean getIs_changed() {
        return is_changed;
    }

    public void setIs_changed(boolean is_changed) {
        this.is_changed = is_changed;
    }

    public void setIs_upload(boolean is_upload) {
        this.is_upload = is_upload;
    }

    public boolean isIs_upload() {
        return is_upload;
    }

    public void setObject_id(int object_id) {
        this.object_id = object_id;
    }

    public int getObject_id() {
        return object_id;
    }
}

package cbt.accreditation.models;

import java.io.Serializable;

/**
 * Created by Dastan on 05.12.2017.
 */

public class User implements Serializable {
    String login, password;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}

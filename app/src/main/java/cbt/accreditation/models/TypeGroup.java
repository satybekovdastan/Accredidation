package cbt.accreditation.models;

/**
 * Created by User on 25.04.2018.
 */

public class TypeGroup {

    private int criteria_id;

    String criteria;

    private String criteria_group;

    private int is_checked;

    private double score;

    private int status;


    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public void setIs_checked(int is_checked) {
        this.is_checked = is_checked;
    }


    public String getCriteria() {
        return criteria;
    }

    public int getIs_checked() {
        return is_checked;
    }

    public String getCriteria_group() {
        return criteria_group;
    }

    public void setCriteria_group(String criteria_group) {
        this.criteria_group = criteria_group;
    }

    public int getCriteria_id() {
        return criteria_id;
    }

    public void setCriteria_id(int criteria_id) {
        this.criteria_id = criteria_id;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}

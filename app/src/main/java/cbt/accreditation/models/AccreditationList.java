package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AccreditationList {
 
    @SerializedName("accreditations")
    @Expose
    private ArrayList<Accreditation> accreditations = null;

    public ArrayList<Accreditation> getAccreditation() { return  accreditations;}

    public void setAccreditation(ArrayList<Accreditation>accreditations){this.accreditations = accreditations;}

}
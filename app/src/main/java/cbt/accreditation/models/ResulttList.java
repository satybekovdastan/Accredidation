package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResulttList {

    @SerializedName("result")
    @Expose
    private ArrayList<AccreditationList> users = null;

    public ArrayList<AccreditationList> getAccreditation() { return  users;}

    public void setAccreditation(ArrayList<AccreditationList>users){this.users = users;}
}
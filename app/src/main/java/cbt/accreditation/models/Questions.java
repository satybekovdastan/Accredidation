package cbt.accreditation.models;

import io.realm.RealmObject;

/**
 * Created by User on 25.04.2018.
 */

public class Questions  extends RealmObject {

    int id;
    String title;
    String answer;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}

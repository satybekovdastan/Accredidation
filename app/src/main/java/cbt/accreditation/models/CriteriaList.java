package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CriteriaList extends RealmObject {

    @PrimaryKey
    @SerializedName("hotelId")
    @Expose
    private int hotelId;
 
    @SerializedName("criterias")
    @Expose
    private RealmList<Type> criterias = null;

    @SerializedName("status")
    @Expose
    private String status = null;

    public RealmList<Type> getAccreditation() { return  criterias;}

    public void setAccreditation(RealmList<Type>criterias){this.criterias = criterias;}

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
                this.hotelId = hotelId;
    }
}
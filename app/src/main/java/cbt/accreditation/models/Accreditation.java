package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by User on 25.04.2018.
 */

public class Accreditation extends RealmObject {

    @SerializedName("accreditation_name")
    @Expose
    String accreditation_name;

    @SerializedName("accreditation_id")
    @Expose
    private int accreditation_id;

    @SerializedName("cbts")
    @Expose
    RealmList<Cbt> cbtArrayList = new RealmList<>();

    @SerializedName("status")
    @Expose
    String status;

    public void setName(String name) {
        this.accreditation_name = name;
    }

    public String getName() {
        return accreditation_name;
    }

    public void setSvtArrayList(RealmList<Cbt> typeArrayList) {
        this.cbtArrayList = typeArrayList;
    }

    public RealmList<Cbt> getSvtArrayList() {
        return cbtArrayList;
    }

    public void addHotel(Cbt hotel){
        cbtArrayList.add(hotel);
    }

    public Cbt getHotel(){
        return cbtArrayList.get(0);
    }

    public void setId(int id) {
        this.accreditation_id = id;
    }

    public int getId() {
        return accreditation_id;
    }

    public void setCbtArrayList(RealmList<Cbt> cbtArrayList) {
        this.cbtArrayList = cbtArrayList;
    }

    public RealmList<Cbt> getCbtArrayList() {
        return cbtArrayList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

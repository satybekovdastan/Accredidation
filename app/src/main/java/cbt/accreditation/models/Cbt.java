package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by User on 25.04.2018.
 */

public class Cbt extends RealmObject{

    @SerializedName("cbt_id")
    @Expose
    int cbt_id;

    @SerializedName("cbt_name")
    @Expose
    String cbt_name;

    @SerializedName("objects_quantity")
    @Expose
    String objects_quantity;

    @SerializedName("user_can_change")
    @Expose
    boolean user_can_change;

    @SerializedName("objects")
    @Expose
    RealmList<Hotel> hotelsArrayList = new RealmList<>();


    public void setName(String name) {
        this.cbt_name = name;
    }

    public String getName() {
        return cbt_name;
    }

    public void setSvtArrayList(RealmList<Hotel> typeArrayList) {
        this.hotelsArrayList = typeArrayList;
    }

    public RealmList<Hotel> getSvtArrayList() {
        return hotelsArrayList;
    }

    public void addHotel(Hotel hotel){
        hotelsArrayList.add(hotel);
    }

    public Hotel getHotel(){
        return hotelsArrayList.get(0);
    }

    public void setHotelsArrayList(RealmList<Hotel> hotelsArrayList) {
        this.hotelsArrayList = hotelsArrayList;
    }

    public RealmList<Hotel> getHotelsArrayList() {
        return hotelsArrayList;
    }

    public void setCbt_id(int cbt_id) {
        this.cbt_id = cbt_id;
    }

    public int getCbt_id() {
        return cbt_id;
    }

    public void setCbt_name(String cbt_name) {
        this.cbt_name = cbt_name;
    }

    public String getCbt_name() {
        return cbt_name;
    }

    public void setObjects_quantity(String objects_quantity) {
        this.objects_quantity = objects_quantity;
    }

    public String getObjects_quantity() {
        return objects_quantity;
    }

    public void setUser_can_change(boolean user_can_change) {
        this.user_can_change = user_can_change;
    }

    public boolean isUser_can_change() {
        return user_can_change;
    }
}

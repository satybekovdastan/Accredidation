package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by User on 25.04.2018.
 */

public class Type  extends RealmObject {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("criteria")
    @Expose
    String criteria;

    @SerializedName("criteria_group")
    @Expose
    private String criteria_group;

    @SerializedName("is_checked")
    @Expose
    private int is_checked;

    @SerializedName("score")
    @Expose
    private double score;


    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public void setIs_checked(int is_checked) {
        this.is_checked = is_checked;
    }


    public String getCriteria() {
        return criteria;
    }

    public int getIs_checked() {
        return is_checked;
    }

    public String getCriteria_group() {
        return criteria_group;
    }

    public void setCriteria_group(String criteria_group) {
        this.criteria_group = criteria_group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }
}

package cbt.accreditation.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dastan on 05.12.2017.
 */

public class Status {
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("success")
    @Expose
    private String success;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSuccess() {
        return success;
    }
}

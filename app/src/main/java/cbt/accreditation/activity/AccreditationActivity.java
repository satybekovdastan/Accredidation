package cbt.accreditation.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cbt.accreditation.R;
import cbt.accreditation.RealmManager;
import cbt.accreditation.TokenPrefsHelper;
import cbt.accreditation.adapters.RVMainAdapter;
import cbt.accreditation.api.ApiService;
import cbt.accreditation.api.ServiceGenerator;
import cbt.accreditation.models.Accreditation;
import cbt.accreditation.models.AccreditationList;
import cbt.accreditation.models.CriteriaList;
import cbt.accreditation.models.Hotel;
import cbt.accreditation.models.Status;
import cbt.accreditation.models.Type;
import cbt.accreditation.models.User;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccreditationActivity extends AppCompatActivity {

    Realm realm;
    RecyclerView recycler;
    RVMainAdapter rvMainAdapter;
    TokenPrefsHelper tokenPrefsHelper = new TokenPrefsHelper();
    User user;
    List<Accreditation> accreditations;
    ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accreditation);

        Gson gson = new Gson();
        String key = tokenPrefsHelper.getValue(this);
        user = gson.fromJson(key, User.class);
        recycler = findViewById(R.id.recycler);

        recycler.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        realm = Realm.getDefaultInstance();

        if (realm.isEmpty()) {
            initViewJson();
        } else {
            accreditations = realm.where(Accreditation.class).findAll();
            rvMainAdapter = new RVMainAdapter(AccreditationActivity.this, accreditations);
            recycler.setAdapter(rvMainAdapter);
        }
    }

    public void initViewJson() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Загрузка...");
        progressBar.show();
        ApiService api = ServiceGenerator.createService(ApiService.class, user.getLogin(), user.getPassword());
        Call<AccreditationList> call;
        call = api.getMyJSON();
        call.enqueue(new Callback<AccreditationList>() {
            @Override
            public void onResponse(Call<AccreditationList> call, Response<AccreditationList> response) {
                if (response.isSuccessful()) {
                    List<Accreditation> users = response.body().getAccreditation();
                    RealmManager.getInstance().addAccreditation(users);
                    getCriteria();
                }
            }

            @Override
            public void onFailure(Call<AccreditationList> call, Throwable t) {
                Log.e("TAG", "FAIL " + t.toString() + "\n");
                Toast.makeText(AccreditationActivity.this, "Проверьте интернет-подключение...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getCriteria() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getCriterias();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            tokenPrefsHelper.save(AccreditationActivity.this, user.getLogin(), user.getPassword());
                            accreditations = realm.where(Accreditation.class).findAll();
                            rvMainAdapter = new RVMainAdapter(AccreditationActivity.this, accreditations);
                            recycler.setAdapter(rvMainAdapter);
                            Toast.makeText(AccreditationActivity.this, "Успешная синхронизация", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            Toast.makeText(AccreditationActivity.this, "Ошибка синхронизации", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        }).start();

    }

    private void getCriterias() throws Exception {
        Realm realm = Realm.getDefaultInstance();
        List<Hotel> result = realm.where(Hotel.class)
                .findAll();
        ApiService api = ServiceGenerator.createService(ApiService.class,
                user.getLogin(),
                user.getPassword());
        Call<CriteriaList> call;
        for (Hotel hotel : result) {
            call = api.getMyCriteria(hotel.getObject_id());
            Response<CriteriaList> response = call.execute();
            if (response.isSuccessful()) {
                RealmList<Type> criteries = response.body().getAccreditation();
                if (response.body().getStatus() == null || response.body().getStatus().isEmpty() || response.body().getStatus().equals("")) {
                    if (criteries != null) {
                        RealmManager.getInstance().addCriteria(hotel.getObject_id(), criteries);
                    }
                }
            } else {
                throw new Exception();
            }
        }

    }

    public void onClick(View view) {
        List<Hotel> result = realm.where(Hotel.class).equalTo("is_upload", true).findAll();
        if (result.size() == 0) {
            Toast.makeText(this, "Изменения не произошло!", Toast.LENGTH_SHORT).show();
        } else {
            if (initIntertnet(AccreditationActivity.this)) {
                postCriteria();
            } else {
                Toast.makeText(this, "Проверьте интернет!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean getCompleted(RealmResults<CriteriaList> criteriaLists) {
        RealmList<Type> resultgroupArraylist = criteriaLists.get(0).getAccreditation();
        assert criteriaLists.get(0) != null;
        resultgroupArraylist = criteriaLists.get(0).getAccreditation();
        boolean status = true;
        Log.e("criteriaLists - ", criteriaLists.size() + "");
        if (criteriaLists.size() == 0) {
            status = false;
        } else {
            for (int i = 0; i < resultgroupArraylist.size(); i++) {
                for (int j = 0; j < resultgroupArraylist.size(); j++) {
                    assert resultgroupArraylist.get(i) != null;
                    if (resultgroupArraylist.get(i).getCriteria_group().equals(resultgroupArraylist.get(j).getCriteria_group())) {
                        if (resultgroupArraylist.get(j).getIs_checked() == 0) {
                            status = false;
                        }
                    }
                }
            }
        }
        return status;
    }



    public static boolean initIntertnet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_research) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            if (initIntertnet(AccreditationActivity.this)) {
                                Realm realm = Realm.getDefaultInstance();
                                realm.beginTransaction();
                                realm.deleteAll();
                                realm.commitTransaction();
                                initViewJson();
                            } else {
                                Toast.makeText(AccreditationActivity.this, "Проверьте интернет!", Toast.LENGTH_SHORT).show();
                            }

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Вы уверены что хотите обновить?").setPositiveButton("ОБНОВИТЬ", dialogClickListener)
                    .setNegativeButton("ОТМЕНА", dialogClickListener).show();
        }

        if (id == R.id.action_logout) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.deleteAll();
                            realm.commitTransaction();
                            tokenPrefsHelper.getDeleteToken(AccreditationActivity.this);
                            startActivity(new Intent(AccreditationActivity.this, LoginActivity.class));
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Вы уверены что хотите выйти?").setPositiveButton("ВЫЙТИ", dialogClickListener)
                    .setNegativeButton("ОТМЕНА", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }


    public void postCriteria() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Загрузка...");
        progressBar.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    postCriterias();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.deleteAll();
                            realm.commitTransaction();
                            initViewJson();
                            Toast.makeText(AccreditationActivity.this, "Успешная отправлено!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            Toast.makeText(AccreditationActivity.this, "Ошибка отправки!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        }).start();

    }

    private void postCriterias() throws Exception {
        Realm realm = Realm.getDefaultInstance();
        List<Hotel> result = realm.where(Hotel.class).equalTo("is_upload", true).findAll();

        ApiService api = ServiceGenerator.createService(ApiService.class,
                user.getLogin(),
                user.getPassword());
        Call<ResponseBody> call;

        for (Hotel hotel : result) {
            RealmResults<CriteriaList> criteriaLists = realm.where(CriteriaList.class)
                    .equalTo("hotelId", hotel.getObject_id())
                    .findAll();
            assert criteriaLists.get(0) != null;
            String reslutAsString = new Gson().toJson(realm.copyFromRealm(criteriaLists.get(0).getAccreditation()));
            boolean is_changed = getCompleted(criteriaLists);
            JSONArray array = null;
            JSONObject object = null;
            try {
                array = new JSONArray(reslutAsString);
                object = new JSONObject();
                object.put("comment", hotel.getComment());
                object.put("is_changed", is_changed);
                object.put("criterias", array);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), object.toString());

            call = api.postMyCriteria(hotel.getObject_id(), requestBody);
            Response<ResponseBody> response = call.execute();
            if (response.isSuccessful()) {
                Log.e("Responce : ", response.body().toString()+" - "+response.body().string());
            } else {
                throw new Exception();
            }
        }
    }

}

package cbt.accreditation.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;

import cbt.accreditation.R;
import cbt.accreditation.adapters.RVHotelAdapter;
import cbt.accreditation.models.Accreditation;
import cbt.accreditation.models.Cbt;
import io.realm.Realm;
import io.realm.RealmList;

public class HotelActivity extends AppCompatActivity {

    RecyclerView recycler;
    RVHotelAdapter rvHotelAdapter;
    boolean change;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String title = getIntent().getStringExtra("title");
        change = getIntent().getBooleanExtra("change", true);
        getSupportActionBar().setTitle(title);
        recycler = findViewById(R.id.recycler);

        recycler.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        initRealm();

    }

    private void initRealm() {
        Realm realm = Realm.getDefaultInstance();
        int idd = getIntent().getIntExtra("idd", 0);
        int idAccreditation = getIntent().getIntExtra("idAccreditation", 0);

        Accreditation accreditation =  realm.where(Accreditation.class)
                .equalTo("accreditation_id", idAccreditation)
                .findFirst();

        RealmList<Cbt> cbts = accreditation.getCbtArrayList();
        for (int i = 0; i < cbts.size(); i++) {
            if (cbts.get(i).getCbt_id()==idd){
                Cbt cbt = cbts.get(i);
                if (cbt != null) {
                    rvHotelAdapter = new RVHotelAdapter(this, cbt.getSvtArrayList(), change);
                    recycler.setAdapter(rvHotelAdapter);
                }
            }
        }
//        Cbt cbt = realm.where(Cbt.class)
//                .equalTo("cbt_id", idd)
//                .findFirst();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode==123){
                runOnUiThread(new Runnable() {
                    @SuppressLint("NewApi")
                    @Override
                    public void run() {
                        initRealm();
                    }
                });
            }
        }}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}

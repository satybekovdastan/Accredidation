package cbt.accreditation.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cbt.accreditation.R;
import cbt.accreditation.RealmManager;
import cbt.accreditation.TokenPrefsHelper;
import cbt.accreditation.adapters.RVTypelAdapter;
import cbt.accreditation.api.ApiService;
import cbt.accreditation.api.ServiceGenerator;
import cbt.accreditation.models.CriteriaList;
import cbt.accreditation.models.Hotel;
import cbt.accreditation.models.Status;
import cbt.accreditation.models.Type;
import cbt.accreditation.models.TypeGroup;
import cbt.accreditation.models.User;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class TypeActivity extends AppCompatActivity {

    RecyclerView recycler;
    RVTypelAdapter rvTypelAdapter;
    TextView textCount, textFio, textInfo;
    EditText editComment;
    Button btnSaveComment;
    Button btnPost;
    int idd;
    boolean changed;
    TokenPrefsHelper tokenPrefsHelper = new TokenPrefsHelper();
    User user;
    ProgressDialog progressBar;
    boolean is_changed;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Gson gson = new Gson();
        String key = tokenPrefsHelper.getValue(this);
        user = gson.fromJson(key, User.class);
        String title = getIntent().getStringExtra("title");
        idd = getIntent().getIntExtra("id", 0);
        changed = getIntent().getBooleanExtra("changed", false);
        getSupportActionBar().setTitle(title);
        recycler = findViewById(R.id.recycler);
        textCount = findViewById(R.id.textCount);
        textInfo = findViewById(R.id.textInfo);
        textFio = findViewById(R.id.textFio);
        editComment = findViewById(R.id.editComment);
        btnSaveComment = findViewById(R.id.btnSaveComment);
        btnPost = findViewById(R.id.btnPost);
        recycler.setNestedScrollingEnabled(false);
        recycler.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);
        recycler.setFocusable(false);
        recycler.requestFocus();
        if (changed) {
            editComment.setInputType(InputType.TYPE_NULL);
            btnSaveComment.setVisibility(View.GONE);
            btnPost.setVisibility(View.GONE);
        }
        btnSaveComment.setVisibility(View.GONE);
        btnSaveComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmManager realmManager = RealmManager.getInstance();
                realmManager.updateComment(idd, editComment.getText().toString());
                Toast.makeText(TypeActivity.this, "Сохранен!", Toast.LENGTH_SHORT).show();
            }
        });

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Realm realm = Realm.getDefaultInstance();
                CriteriaList criteriaList = realm.where(CriteriaList.class)
                        .equalTo("hotelId", idd)
                        .findFirst();
                is_changed = getCompleted(criteriaList);
                if (is_changed == false) {
                    Toast.makeText(TypeActivity.this, "Не завершено!", Toast.LENGTH_SHORT).show();
                } else {
                    postCriteria();
                }
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                initRealm();
            }
        });
    }

    public void postCriteria() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Загрузка...");
        progressBar.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    postCriterias();
                    runOnUiThread(new Runnable() {
                        @SuppressLint("NewApi")
                        @Override
                        public void run() {
                            RealmManager realmManager = RealmManager.getInstance();
                            realmManager.updateIsChange(idd, is_changed, editComment.getText().toString());
                            changed = true;
                            btnPost.setVisibility(View.GONE);
                            editComment.setInputType(InputType.TYPE_NULL);
                            progressBar.dismiss();
                            Toast.makeText(TypeActivity.this, "Успешная отправлено!", Toast.LENGTH_SHORT).show();
                            initRealm();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            Toast.makeText(TypeActivity.this, "Ошибка отправки!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        }).start();

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initRealm() {
        Realm realm = Realm.getDefaultInstance();

        Hotel hotel = realm.where(Hotel.class)
                .equalTo("object_id", idd)
                .findFirst();

        Log.e("RES", hotel + "  " + idd);
        textFio.setText(hotel.getObject_fio());
        textInfo.setText(Html.fromHtml(hotel.getObject_info()));
        editComment.setText(hotel.getComment());
        double count = 0;
        double count2 = 0;

        CriteriaList criteriaList = realm.where(CriteriaList.class)
                .equalTo("hotelId", idd)
                .findFirst();

        RealmList<Type> resultgroupArraylist;
        resultgroupArraylist = criteriaList.getAccreditation();

        ArrayList<TypeGroup> group = new ArrayList<>();
        boolean found = false;

        for (int i = 0; i < resultgroupArraylist.size(); i++) {
            if (resultgroupArraylist.get(i).getIs_checked() == 2) {
                double c = resultgroupArraylist.get(i).getScore();
                count = count2 + c;
                count2 = count;
            }
            if (group.isEmpty()) {
                TypeGroup typeGroup = new TypeGroup();
                typeGroup.setCriteria_group(resultgroupArraylist.get(i).getCriteria_group());
                typeGroup.setCriteria(resultgroupArraylist.get(i).getCriteria());
                typeGroup.setCriteria_id(resultgroupArraylist.get(i).getId());

                boolean status = false;

                for (int j = 0; j < resultgroupArraylist.size(); j++) {
                    if (resultgroupArraylist.get(i).getCriteria_group().equals(resultgroupArraylist.get(j).getCriteria_group())) {
                        if (resultgroupArraylist.get(j).getIs_checked() == 0) {
                            status = true;
                        }
                    }
                }

                if (status) {
                    typeGroup.setStatus(0);
                } else {
                    typeGroup.setStatus(1);
                }

                group.add(typeGroup);
            } else {
                for (int n = 0; n < group.size(); n++) {
                    Log.e("TAGG", "onCreate: " + resultgroupArraylist.get(i).getCriteria_group());
                    if (Objects.equals(group.get(n).getCriteria_group(), resultgroupArraylist.get(i).getCriteria_group())) {
                        found = true;
                        n = group.size();
                    }
                }
                if (!found) {
                    TypeGroup typeGroup = new TypeGroup();
                    typeGroup.setCriteria_group(resultgroupArraylist.get(i).getCriteria_group());
                    typeGroup.setCriteria(resultgroupArraylist.get(i).getCriteria());
                    typeGroup.setCriteria_id(resultgroupArraylist.get(i).getId());
                    boolean status = false;

                    for (int j = 0; j < resultgroupArraylist.size(); j++) {
                        if (resultgroupArraylist.get(i).getCriteria_group().equals(resultgroupArraylist.get(j).getCriteria_group())) {
                            if (resultgroupArraylist.get(j).getIs_checked() == 0) {
                                status = true;
                            }
                        }
                    }

                    if (status) {
                        typeGroup.setStatus(0);
                    } else {
                        typeGroup.setStatus(1);
                    }
                    group.add(typeGroup);
                } else {
                    found = false;
                }
            }

            if (count <= 49.99) {
                textCount.setText("0 эдельвейс  - " + count + " баллов");
            } else if (count >= 50 && count <= 64.99) {
                textCount.setText("1 эдельвейс  - " + count + " баллов");
            } else if (count >= 65 && count <= 84.99) {
                textCount.setText("2 эдельвейс  - " + count + " баллов");
            } else if (count >= 85) {
                textCount.setText("3 эдельвейс  - " + count + " баллов");
            }

        }
        rvTypelAdapter = new RVTypelAdapter(this, group, idd, changed);
        recycler.setAdapter(rvTypelAdapter);
    }

    private void postCriterias() throws Exception {
        Realm realm = Realm.getDefaultInstance();

        ApiService api = ServiceGenerator.createService(ApiService.class,
                user.getLogin(),
                user.getPassword());
        Call<ResponseBody> call;

        CriteriaList criteriaList = realm.where(CriteriaList.class)
                .equalTo("hotelId", idd)
                .findFirst();

        String reslutAsString = new Gson().toJson(realm.copyFromRealm(criteriaList.getAccreditation()));

        JSONArray array = null;
        JSONObject object = null;
        try {
            array = new JSONArray(reslutAsString);
            object = new JSONObject();
            object.put("comment", editComment.getText().toString());
            object.put("is_changed", is_changed);
            object.put("criterias", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), object.toString());

        call = api.postMyCriteria(idd, requestBody);
        Response<ResponseBody> response = call.execute();
        if (response.isSuccessful()) {
            Log.e("Responce : ", response.body().toString()+" - "+response.body().string());

        } else {
            throw new Exception();
        }
    }

    private boolean getCompleted(CriteriaList criteriaLists) {

        RealmList<Type> resultgroupArraylist = criteriaLists.getAccreditation();
        resultgroupArraylist = criteriaLists.getAccreditation();
        boolean status = true;
        for (int i = 0; i < resultgroupArraylist.size(); i++) {
            for (int j = 0; j < resultgroupArraylist.size(); j++) {
                if (resultgroupArraylist.get(i).getCriteria_group().equals(resultgroupArraylist.get(j).getCriteria_group())) {
                    if (resultgroupArraylist.get(j).getIs_checked() == 0) {
                        status = false;
                    }
                }

            }
        }
        return status;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 123) {
                runOnUiThread(new Runnable() {
                    @SuppressLint("NewApi")
                    @Override
                    public void run() {
                        RealmManager realmManager = RealmManager.getInstance();
                        realmManager.updateComment(idd, editComment.getText().toString());
                        initRealm();
                    }
                });
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            RealmManager realmManager = RealmManager.getInstance();
            realmManager.updateComment(idd, editComment.getText().toString());
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        RealmManager realmManager = RealmManager.getInstance();
        realmManager.updateComment(idd, editComment.getText().toString());
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }
}

package cbt.accreditation.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import cbt.accreditation.R;
import cbt.accreditation.RealmManager;
import cbt.accreditation.TokenPrefsHelper;
import cbt.accreditation.api.ApiService;
import cbt.accreditation.api.ServiceGenerator;
import cbt.accreditation.models.Accreditation;
import cbt.accreditation.models.AccreditationList;
import cbt.accreditation.models.CriteriaList;
import cbt.accreditation.models.Hotel;
import cbt.accreditation.models.Type;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText editLogin, editPassword;
    Button btnLogin;
    Realm realm;
    TokenPrefsHelper tokenPrefsHelper = new TokenPrefsHelper();
    ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editLogin = findViewById(R.id.editLogin);
        editPassword = findViewById(R.id.editPassword);
        btnLogin = findViewById(R.id.btnLogin);
        realm = Realm.getDefaultInstance();

        String key = tokenPrefsHelper.getValue(this);
        if (key == null || key.equals("")) {

        } else {
            startActivity(new Intent(this, AccreditationActivity.class));
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean bool = true;
                if (editLogin.getText().toString().isEmpty()) {
                    bool = false;
                    editLogin.setError("Заполните поле");
                }
                if (editPassword.getText().toString().isEmpty()) {
                    bool = false;
                    editPassword.setError("Заполните поле");
                }
                if (initIntertnet(LoginActivity.this)) {
                    if (bool) {
                        initViewJson(editLogin.getText().toString(), editPassword.getText().toString());
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Проверьте интернет!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void initViewJson(final String login, final String password) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Uploading...");
        dialog.show();
        ApiService api = ServiceGenerator.createService(ApiService.class, login, password);
        Call<AccreditationList> call;
        call = api.getMyJSON();
        call.enqueue(new Callback<AccreditationList>() {
            @Override
            public void onResponse(Call<AccreditationList> call, Response<AccreditationList> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getAccreditation().size() == 0) {
                        Toast.makeText(LoginActivity.this, "У вас нет акредитации!", Toast.LENGTH_SHORT).show();
                    } else {
                        if (response.body().getAccreditation().get(0).getStatus() == null || response.body().getAccreditation().get(0).getStatus().isEmpty() || response.body().getAccreditation().get(0).getStatus().equals("")) {
                            List<Accreditation> users = response.body().getAccreditation();
                            RealmManager.getInstance().addAccreditation(users);
                            getCriteria();
                        } else {
                            Log.e("WRONG", response.body().toString());
                            Toast.makeText(LoginActivity.this, "Something went wrong...!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Log.e("RES", response.code() + "");
                    Toast.makeText(LoginActivity.this, "Неправильный Логин или Пароль!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AccreditationList> call, Throwable t) {
                Log.e("TAG", "FAIL " + t.toString() + "\n");
            }
        });
    }

    public void getCriteria() {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Загрузка...");
        progressBar.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getCriterias();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            tokenPrefsHelper.save(LoginActivity.this, editLogin.getText().toString(), editPassword.getText().toString());
                            startActivity(new Intent(LoginActivity.this, AccreditationActivity.class));
                            finish();
                            Toast.makeText(LoginActivity.this, "Успешная синхронизация", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.dismiss();
                            Toast.makeText(LoginActivity.this, "Ошибка синхронизации", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        }).start();

    }

    private void getCriterias() throws Exception {
        Realm realm = Realm.getDefaultInstance();
        List<Hotel> result = realm.where(Hotel.class)
                .findAll();
        ApiService api = ServiceGenerator.createService(ApiService.class,
                editLogin.getText().toString(),
                editPassword.getText().toString());
        Call<CriteriaList> call;
        for (Hotel hotel : result) {
            call = api.getMyCriteria(hotel.getObject_id());
            Log.e("BAZA", "run: " + hotel.getObject_id());
            Response<CriteriaList> response = call.execute();
            if (response.isSuccessful()) {
                RealmList<Type> criteries = response.body().getAccreditation();
                Log.e("STATUS", hotel.getObject_id() + " " + response.body().getStatus() + " + " + criteries);
                if (response.body().getStatus() == null || response.body().getStatus().isEmpty() || response.body().getStatus().equals("")) {
                    if (criteries != null) {
                        RealmManager.getInstance().addCriteria(hotel.getObject_id(), criteries);
                    }
                }
            } else {
                throw new Exception();
            }
        }

    }

    public static boolean initIntertnet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }



}

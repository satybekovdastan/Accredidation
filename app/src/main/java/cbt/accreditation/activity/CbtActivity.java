package cbt.accreditation.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import cbt.accreditation.R;
import cbt.accreditation.adapters.RVCbtAdapter;
import cbt.accreditation.models.Accreditation;
import io.realm.Realm;

public class CbtActivity extends AppCompatActivity {

    RecyclerView recycler;
    RVCbtAdapter rvCbtAdapter;
    int idd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        idd = getIntent().getIntExtra("id", 0);
        String title = getIntent().getStringExtra("title");
        getSupportActionBar().setTitle(title);
        recycler = findViewById(R.id.recycler);

        recycler.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        Realm realm = Realm.getDefaultInstance();

        Accreditation accreditation = realm.where(Accreditation.class)
                .equalTo("accreditation_id", idd)
                .findFirst();

        rvCbtAdapter = new RVCbtAdapter(this, accreditation.getSvtArrayList(), idd);
        recycler.setAdapter(rvCbtAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}

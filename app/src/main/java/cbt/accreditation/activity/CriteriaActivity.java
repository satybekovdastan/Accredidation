package cbt.accreditation.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;

import cbt.accreditation.R;
import cbt.accreditation.adapters.RVCriterialAdapter;
import cbt.accreditation.models.CriteriaList;
import cbt.accreditation.models.Type;
import io.realm.Realm;
import io.realm.RealmList;

public class CriteriaActivity extends AppCompatActivity {

    RecyclerView recycler;
    RVCriterialAdapter rvCriterialAdapter;
    boolean changed;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        int idd = getIntent().getIntExtra("id", 0);
        String group = getIntent().getStringExtra("group");
        String title = getIntent().getStringExtra("title");
        changed = getIntent().getBooleanExtra("changed", false);
        getSupportActionBar().setTitle(title);
        recycler = findViewById(R.id.recycler);

        recycler.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        Realm realm = Realm.getDefaultInstance();
        Log.e("RES", group + "  " + idd);

        CriteriaList criteriaList = realm.where(CriteriaList.class)
                .equalTo("hotelId", idd)
                .findFirst();

        RealmList<Type> typeRealmList;
        typeRealmList = criteriaList.getAccreditation();

        ArrayList<Type> types = new ArrayList<>();
        for (int i = 0; i < typeRealmList.size(); i++) {
            if (typeRealmList.get(i).getCriteria_group().equals(group)) {
                types.add(typeRealmList.get(i));
            }
        }

        rvCriterialAdapter = new RVCriterialAdapter(this, types, idd, changed);
        recycler.setAdapter(rvCriterialAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }
}

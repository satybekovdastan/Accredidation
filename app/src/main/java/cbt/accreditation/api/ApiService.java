package cbt.accreditation.api;

import cbt.accreditation.models.AccreditationList;
import cbt.accreditation.models.CriteriaList;
import cbt.accreditation.models.Status;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @GET("list/")
    Call<AccreditationList> getMyJSON();

    @GET("single/{id}")
    Call<CriteriaList> getMyCriteria(@Path("id") int id);

    @Headers("Content-Type: application/json")
    @POST("list/")
    Call<Status> setMyJSON(@Body RequestBody object);

    @Headers("Content-Type: application/json")
    @POST("single/{id}")
    Call<ResponseBody> postMyCriteria(@Path("id") int id, @Body RequestBody object);

}

package cbt.accreditation;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import cbt.accreditation.models.User;

/**
 * Created by tanaki on 7/2/16.
 */
public class TokenPrefsHelper {

    public static String PREFS_NAME = "Prefs";
    public static String PREFS_LOGIN = "Login";

    public TokenPrefsHelper() {
        super();
    }

    public void save(Context context, String login, String password) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        editor = settings.edit();

        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        Gson gson = new Gson();
        String json = gson.toJson(user);

        editor.putString(PREFS_LOGIN, json);
        editor.commit();
    }

    public String getValue(Context context) {
        SharedPreferences settings = null;
        String value = null;
        try {
            settings = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
            value = settings.getString(PREFS_LOGIN, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public void getDeleteToken(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);
        editor = settings.edit();
        editor.remove(PREFS_LOGIN);
        editor.commit();
    }


}

package cbt.accreditation.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cbt.accreditation.activity.CriteriaActivity;
import cbt.accreditation.R;
import cbt.accreditation.models.TypeGroup;

public class RVTypelAdapter extends RecyclerView.Adapter<RVTypelAdapter.PersonViewHolder> {

    Context context;
    TypeGroup vse;


    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        RelativeLayout layout_partner;

        PersonViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.nameText);
            layout_partner = itemView.findViewById(R.id.layout_partner);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    vse = listVse.get(getAdapterPosition());
                    Intent intent = new Intent(context, CriteriaActivity.class);
                    intent.putExtra("group", vse.getCriteria_group());
                    intent.putExtra("title", vse.getCriteria_group());
                    intent.putExtra("id", object_id);
                    intent.putExtra("changed", changed);
                    Activity activity = (Activity) context;
                    activity.startActivityForResult(intent, 123);
                }
            });

        }

    }

    List<TypeGroup> listVse;
    int object_id;
    boolean changed;

    public RVTypelAdapter(Context context, List<TypeGroup> listVse, int object_id, boolean changed) {
        this.listVse = listVse;
        this.context = context;
        this.object_id = object_id;
        this.changed = changed;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_type, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        vse = new TypeGroup();
        vse = listVse.get(i);
        Log.e("STATUS", vse.getStatus()+"");
        personViewHolder.title.setText(vse.getCriteria_group());

        if (vse.getStatus()!=0){
            personViewHolder.layout_partner.setVisibility(View.VISIBLE);
            personViewHolder.layout_partner.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
        }else {
            personViewHolder.layout_partner.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}
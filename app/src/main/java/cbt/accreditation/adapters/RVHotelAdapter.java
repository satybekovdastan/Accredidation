package cbt.accreditation.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cbt.accreditation.R;
import cbt.accreditation.activity.TypeActivity;
import cbt.accreditation.models.CriteriaList;
import cbt.accreditation.models.Hotel;
import cbt.accreditation.models.Type;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class RVHotelAdapter extends RecyclerView.Adapter<RVHotelAdapter.PersonViewHolder> {

    private Context context;
    private Hotel vse;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        RelativeLayout layout_partner;

        PersonViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.nameText);
            layout_partner = itemView.findViewById(R.id.layout_partner);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TypeActivity.class);
                    vse = listVse.get(getAdapterPosition());
                    intent.putExtra("id", vse.getObject_id());
                    intent.putExtra("title", vse.getName());
                    if (!change) {
                        intent.putExtra("changed", true);
                    } else {
                        intent.putExtra("changed", vse.getIs_changed());
                    }
                    Activity activity = (Activity) context;
                    activity.startActivityForResult(intent, 123);
                }
            });
        }
    }


    RealmList<Hotel> listVse;
    boolean change;

    public RVHotelAdapter(Context context, RealmList<Hotel> listVse, boolean change) {
        this.listVse = listVse;
        this.context = context;
        this.change = change;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_main, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        vse = new Hotel();
        vse = listVse.get(i);
        personViewHolder.title.setText(vse.getName());

        boolean bool = false;
        try {
            bool = getCompleted(vse.getObject_id());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (vse.getIs_changed()) {
            personViewHolder.layout_partner.setVisibility(View.VISIBLE);
            personViewHolder.layout_partner.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
        } else {
            personViewHolder.layout_partner.setVisibility(View.GONE);
            if (!bool) {
                personViewHolder.layout_partner.setVisibility(View.VISIBLE);
                personViewHolder.layout_partner.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            }
        }
    }

    private boolean getCompleted(int object_id) {
        boolean status = false;
        try {
            Realm realm = Realm.getDefaultInstance();
            CriteriaList criteriaLists = realm.where(CriteriaList.class)
                    .equalTo("hotelId", object_id)
                    .findFirst();

            RealmList<Type> resultgroupArraylist;
            assert criteriaLists != null;
            resultgroupArraylist = criteriaLists.getAccreditation();

            for (int i = 0; i < resultgroupArraylist.size(); i++) {
                for (int j = 0; j < resultgroupArraylist.size(); j++) {
                    assert resultgroupArraylist.get(i) != null;
                    if (resultgroupArraylist.get(i).getCriteria_group().equals(resultgroupArraylist.get(j).getCriteria_group())) {
                        assert resultgroupArraylist.get(j) != null;
                        if (resultgroupArraylist.get(j).getIs_checked() == 0) {
                            status = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}
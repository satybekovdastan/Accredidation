package cbt.accreditation.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cbt.accreditation.R;
import cbt.accreditation.activity.CbtActivity;
import cbt.accreditation.models.Accreditation;

public class RVMainAdapter extends RecyclerView.Adapter<RVMainAdapter.PersonViewHolder> {

    Context context;
    Accreditation vse;


    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView title;

        PersonViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.nameText);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CbtActivity.class);
                    vse = listVse.get(getAdapterPosition());
                    intent.putExtra("id", vse.getId());
                    intent.putExtra("title", vse.getName());
                    Activity activity = (Activity) context;
                    activity.startActivity(intent);
                }
            });
        }
    }

    List<Accreditation> listVse;

    public RVMainAdapter(Context context, List<Accreditation> listVse) {
        this.listVse = listVse;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_main, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        vse = new Accreditation();
        vse = listVse.get(i);
        personViewHolder.title.setText(vse.getName());
    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}
package cbt.accreditation.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cbt.accreditation.activity.HotelActivity;
import cbt.accreditation.R;
import cbt.accreditation.models.Cbt;
import io.realm.RealmList;

public class RVCbtAdapter extends RecyclerView.Adapter<RVCbtAdapter.PersonViewHolder> {

    Context context;
    Cbt vse;


    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView title,textQuantity;
        RelativeLayout layout_partner;

        PersonViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.nameText);
            textQuantity = itemView.findViewById(R.id.textQuantity);
            layout_partner = itemView.findViewById(R.id.layout_partner);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    vse = listVse.get(getAdapterPosition());
                    Intent intent = new Intent(context, HotelActivity.class);
                    intent.putExtra("idAccreditation", idAccreditation);
                    intent.putExtra("idd", vse.getCbt_id());
                    intent.putExtra("title", vse.getName());
                    intent.putExtra("change", vse.isUser_can_change());
                    Activity activity = (Activity) context;
                    activity.startActivity(intent);
                }
            });

        }
    }

    RealmList<Cbt> listVse;
    int idAccreditation;

    public RVCbtAdapter(Context context, RealmList<Cbt> listVse, int id) {
        this.listVse = listVse;
        this.context = context;
        this.idAccreditation = id;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_cbt, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, int i) {
        vse = new Cbt();
        vse = listVse.get(i);
        Log.e("NAME", vse.getName());
        personViewHolder.title.setText(vse.getName());
        personViewHolder.textQuantity.setText(vse.getObjects_quantity()+"");

        if (!vse.isUser_can_change()){
            personViewHolder.textQuantity.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            personViewHolder.textQuantity.setTextColor(ContextCompat.getColor(context, R.color.white));
        }else {
            personViewHolder.textQuantity.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_font));
            personViewHolder.textQuantity.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }

    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}
package cbt.accreditation.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cbt.accreditation.R;
import cbt.accreditation.RealmManager;
import cbt.accreditation.models.Type;

public class RVCriterialAdapter extends RecyclerView.Adapter<RVCriterialAdapter.PersonViewHolder> {

    Context context;
    Type vse;


    public class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView textYes;
        TextView textNo;

        PersonViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.titleCriteria);

            textYes = itemView.findViewById(R.id.textYes);
            textNo = itemView.findViewById(R.id.textNo);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (changed) {
                        Toast.makeText(context, "Вы не сможете изменить!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    List<Type> listVse;
    int idGroup;
    boolean changed;

    public RVCriterialAdapter(Context context, List<Type> listVse, int idGroup, boolean changed) {
        this.listVse = listVse;
        this.context = context;
        this.idGroup = idGroup;
        this.changed = changed;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_criteria, viewGroup, false);
        return new PersonViewHolder(v);
    }

    private void enableYes(PersonViewHolder viewHolder) {
        viewHolder.textYes.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
        viewHolder.textYes.setTextColor(Color.WHITE);
        viewHolder.textNo.setBackgroundColor(Color.TRANSPARENT);
        viewHolder.textNo.setTextColor(ContextCompat.getColor(context, R.color.textColor));
    }

    private void enableNo(PersonViewHolder viewHolder) {
        viewHolder.textNo.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
        viewHolder.textNo.setTextColor(Color.WHITE);
        viewHolder.textYes.setBackgroundColor(Color.TRANSPARENT);
        viewHolder.textYes.setTextColor(ContextCompat.getColor(context, R.color.textColor));
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder viewHolder, int i) {
        final Type vse = listVse.get(i);
        viewHolder.title.setText(vse.getCriteria());

        if (vse.getIs_checked() == 2) {
            enableYes(viewHolder);
        } else if (vse.getIs_checked() == 1) {
            enableNo(viewHolder);
        } else {
            viewHolder.textNo.setBackgroundColor(Color.TRANSPARENT);
            viewHolder.textYes.setBackgroundColor(Color.TRANSPARENT);
            viewHolder.textYes.setTextColor(ContextCompat.getColor(context, R.color.textColor));
            viewHolder.textNo.setTextColor(ContextCompat.getColor(context, R.color.textColor));
        }

        viewHolder.textYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (changed) {
                    Toast.makeText(context, "Вы не сможете изменить!", Toast.LENGTH_SHORT).show();
                }else {
                    RealmManager.getInstance().updateCriteria2(idGroup, 2, vse.getCriteria(), vse.getId());
                    enableYes(viewHolder);
                }
            }
        });
        viewHolder.textNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (changed) {
                    Toast.makeText(context, "Вы не сможете изменить!", Toast.LENGTH_SHORT).show();
                }else {
                    RealmManager.getInstance().updateCriteria2(idGroup, 1, vse.getCriteria(), vse.getId());
                    enableNo(viewHolder);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listVse.size();
    }

}
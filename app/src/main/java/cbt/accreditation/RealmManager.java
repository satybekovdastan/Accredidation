package cbt.accreditation;

import java.util.List;

import cbt.accreditation.models.Accreditation;
import cbt.accreditation.models.CriteriaList;
import cbt.accreditation.models.Hotel;
import cbt.accreditation.models.Type;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by daniar on 23.11.2017.
 */

public class RealmManager {

    private static volatile RealmManager instance = null;
    private final String TAG = "RealmManager";

    public static RealmManager getInstance() {
        RealmManager localInstance = instance;
        if (localInstance == null) {
            synchronized (RealmManager.class) {
                localInstance = instance;
                if (localInstance == null)
                    instance = localInstance = new RealmManager();
            }
        }
        return localInstance;
    }

    public RealmManager() {
    }

    public void updateCriteria2(int id, int check, String group, int criteriaId) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Hotel hotel = realm.where(Hotel.class).equalTo("object_id", id).findFirst();
        hotel.setIs_upload(true);
        CriteriaList criteriaList = realm.where(CriteriaList.class).equalTo("hotelId", id).findFirst();
        RealmList<Type> types = criteriaList.getAccreditation();
        for (int i = 0; i < types.size(); i++) {
            if (types.get(i).getId()==criteriaId) {
                types.get(i).setIs_checked(check);
            }
        }
        realm.commitTransaction();
        realm.close();
    }

    public void updateComment(int id, String comment) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Hotel hotel = realm.where(Hotel.class).equalTo("object_id", id).findFirst();
        hotel.setIs_upload(true);
        hotel.setComment(comment);

        realm.commitTransaction();
        realm.close();
    }

    public void updateIsChange(int id, boolean is_change, String comment) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        Hotel hotel = realm.where(Hotel.class).equalTo("object_id", id).findFirst();
        hotel.setIs_upload(true);
        hotel.setIs_changed(is_change);
        hotel.setComment(comment);
        realm.commitTransaction();
        realm.close();
    }

    public void addCriteria(int id, RealmList<Type> types) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        CriteriaList criteriaList = new CriteriaList();
        criteriaList.setHotelId(id);
        criteriaList.setAccreditation(types);
        realm.copyToRealmOrUpdate(criteriaList);
        realm.commitTransaction();
        realm.close();
    }

    public void addAccreditation(List<Accreditation> users) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Realm.getDefaultInstance().copyToRealmOrUpdate(users);
        realm.commitTransaction();
        realm.close();
    }

}
